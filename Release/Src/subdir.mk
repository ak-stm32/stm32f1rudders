################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/AK_ADC.c \
../Src/AK_FYQ5641.c \
../Src/AK_TIM2.c \
../Src/AK_USB.c \
../Src/AK_USB_HID_DESC.c \
../Src/main.c \
../Src/stm32f1xx_hal_msp.c \
../Src/stm32f1xx_it.c \
../Src/usb_device.c \
../Src/usbd_conf.c \
../Src/usbd_desc.c 

OBJS += \
./Src/AK_ADC.o \
./Src/AK_FYQ5641.o \
./Src/AK_TIM2.o \
./Src/AK_USB.o \
./Src/AK_USB_HID_DESC.o \
./Src/main.o \
./Src/stm32f1xx_hal_msp.o \
./Src/stm32f1xx_it.o \
./Src/usb_device.o \
./Src/usbd_conf.o \
./Src/usbd_desc.o 

C_DEPS += \
./Src/AK_ADC.d \
./Src/AK_FYQ5641.d \
./Src/AK_TIM2.d \
./Src/AK_USB.d \
./Src/AK_USB_HID_DESC.d \
./Src/main.d \
./Src/stm32f1xx_hal_msp.d \
./Src/stm32f1xx_it.d \
./Src/usb_device.d \
./Src/usbd_conf.d \
./Src/usbd_desc.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g -DAK_USB_HID_DESC -I"/Users/akoiro/_prj/_stm32/stm32f1tach/Inc" -I"/Users/akoiro/_prj/_stm32/stm32f1tach/Drivers/CMSIS/Include" -I"/Users/akoiro/_prj/_stm32/stm32f1tach/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/Users/akoiro/_prj/_stm32/stm32f1tach/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/Users/akoiro/_prj/_stm32/stm32f1tach/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" -I"/Users/akoiro/_prj/_stm32/stm32f1tach/Middlewares/ST/STM32_USB_Device_Library/Core/Src" -I"/Users/akoiro/_prj/_stm32/stm32f1tach/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


