/*
 * Timer2.c
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */
#include <AK_TIM2.h>

TIM_HandleTypeDef htim2;
#define T72
#ifdef T72
#define AK_TIM2_Prescaler 60000
#define AK_TIM2_Period 3
#endif
/**
 * Timer for FYQ5641
 * 8Mhz/20000 = 400Hz - 400Hz is the best for us
 * 72Mhz/60000 = 1200Hz/3 = 400Hz - 400Hz is the best for us
 */
void AK_TIM2_Init(void) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	__TIM2_CLK_ENABLE();
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = AK_TIM2_Prescaler;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = AK_TIM2_Period;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.RepetitionCounter = 0;
	HAL_TIM_Base_Init(&htim2);

	HAL_TIM_Base_Start_IT(&htim2);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig);
}

void AK_TIM2_Msp_Init(void) {
	HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM2_IRQn);
}
