/*
 * ADC.c
 *
 *  Created on: 1 ���� 2016 �.
 *      Author: pervoliner
 */

#include <AK_ADC.h>

extern void Error_Handler(void);
void _GPIO_Init(void);
void _DMA_Init(void);
void _ADC_Init(void);
void _GPIO_Msp_Init(void);
void _DMA_Msp_Init(void);


ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;
uint32_t AK_ADC_Value[3];


void AK_ADC_Init(void) {
	_DMA_Init();
	_GPIO_Init();
	_ADC_Init();
}

void AK_ADC_Msp_Init(void) {
	__HAL_RCC_ADC1_CLK_ENABLE();

	_GPIO_Msp_Init();

	_DMA_Msp_Init();
}

void AK_ADC_Msp_DeInit(void)
{
    __HAL_RCC_ADC1_CLK_DISABLE();

    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_0| GPIO_PIN_1| GPIO_PIN_2);

    HAL_DMA_DeInit(hadc1.DMA_Handle);
}

void AK_ADC_Start(void) {
	HAL_ADC_Start_DMA(&hadc1, AK_ADC_Value, 3);
}

void _GPIO_Init(void) {
	__GPIOA_CLK_ENABLE();
}

void _DMA_Init(void) {
	  /* DMA controller clock enable */
	  __HAL_RCC_DMA1_CLK_ENABLE();

	  /* DMA interrupt init */
	  /* DMA1_Channel1_IRQn interrupt configuration */
	  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
	  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}

void _ADC_Init(void) {
	ADC_ChannelConfTypeDef sConfig;

	/**Common config
	 */
	hadc1.Instance = ADC1;
	hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
	hadc1.Init.ContinuousConvMode = ENABLE;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 3;
	if (HAL_ADC_Init(&hadc1) != HAL_OK) {
		Error_Handler();
	}

	/**Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_0;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		Error_Handler();
	}

	/**Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_1;
	sConfig.Rank = 2;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		Error_Handler();
	}

	/**Configure Regular Channel
	 */
	sConfig.Channel = ADC_CHANNEL_2;
	sConfig.Rank = 3;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		Error_Handler();
	}
}

void _DMA_Msp_Init(void) {
    /* Peripheral DMA init*/

    hdma_adc1.Instance = DMA1_Channel1;
    hdma_adc1.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_adc1.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_adc1.Init.MemInc = DMA_MINC_ENABLE;
    hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
    hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
    hdma_adc1.Init.Mode = DMA_CIRCULAR;
    hdma_adc1.Init.Priority = DMA_PRIORITY_MEDIUM;
    if (HAL_DMA_Init(&hdma_adc1) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(&hadc1, DMA_Handle, hdma_adc1);
}

void _GPIO_Msp_Init(void) {
	/**ADC1 GPIO Configuration
	 PA0-WKUP     ------> ADC1_IN0
	 PA1     ------> ADC1_IN1
	 PA2     ------> ADC1_IN2
	 */
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}
