/*
 * AK_USB.c
 *
 *  Created on: 4 ���� 2016 �.
 *      Author: pervoliner
 */
#include "AK_USB.h"
#include "usb_device.h"

extern uint32_t AK_ADC_Value[3];
extern USBD_HandleTypeDef hUsbDeviceFS;

uint8_t _USB_BUFFER[3][3];

uint8_t _CURRENT_REPORT = 0;

void _USB_Convert(uint32_t value, uint8_t* buffer);

void AK_USB_Init(void) {
	_USB_BUFFER[0][0] = 0x01;
	_USB_BUFFER[1][0] = 0x02;
	_USB_BUFFER[2][0] = 0x03;
}

void AK_USB_Send(void) {
	_USB_Convert(AK_ADC_Value[_CURRENT_REPORT], _USB_BUFFER[_CURRENT_REPORT]);
	USBD_HID_SendReport(&hUsbDeviceFS, _USB_BUFFER[_CURRENT_REPORT], 3);

	_CURRENT_REPORT++;
	if (_CURRENT_REPORT > 2)
		_CURRENT_REPORT = 0;
}

void _USB_Convert(uint32_t value, uint8_t* buffer) {
	buffer[1] = ((value >> 0) & 0xff); //value0 lo bites
	buffer[2] = ((value >> 8) & 0xff); //value0 hi bites
}

//void send2USB(void)
//{
//	//trace_printf("value0: %u -- value1: %u -- value2: %u\n ",value0, value1, value2);
//
//	static uint8_t order = 0;
//	static uint8_t buff0[3];
//	static uint8_t buff1[3];
//	static uint8_t buff2[3];
//
//	//report id
//
//	switch (order)
//	{
//		case 0:
//			buff0[1] = ((ADC_Value[0] >> 0) & 0xff); //value0 lo bites
//			buff0[2] = ((ADC_Value[0] >> 8) & 0xff); //value0 hi bites
//			USBD_HID_SendReport (&hUsbDeviceFS,buff0,3);
//			order = 1;
//			break;
//		case 1:
//			buff1[1] = ((ADC_Value[1] >> 0) & 0xff); //value0 lo bites
//			buff1[2] = ((ADC_Value[1] >> 8) & 0xff); //value0 hi bites
//			USBD_HID_SendReport (&hUsbDeviceFS,buff1,3);
//			order = 2;
//			break;
//		case 2:
//			buff2[1] = ((ADC_Value[2] >> 0) & 0xff); //value0 lo bites
//			buff2[2] = ((ADC_Value[2] >> 8) & 0xff); //value0 hi bites
//			USBD_HID_SendReport (&hUsbDeviceFS,buff2,3);
//			order = 0;
//			break;
//	}
//
////
////	buff[5] = ((value1 >> 0) & 0xff); //value0 lo bites
////	buff[6] = ((value1 >> 8) & 0xff); //value0 hi bites
//
//
//}
