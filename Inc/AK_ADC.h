/*
 * ADC.h
 *
 *  Created on: 1 ���� 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_ADC_H_
#define AK_ADC_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"

void AK_ADC_Init(void);
void AK_ADC_Msp_Init(void);
void AK_ADC_Msp_DeInit(void);
void AK_ADC_Start(void);


#endif /* AK_ADC_H_ */
