/*
 * FYQ5641.h
 *
 *  Created on: 30 ���. 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_FYQ5641_H_
#define AK_FYQ5641_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"

#define TYPE_A

//pin 11
#define A_PIN	GPIO_PIN_0
//pin 7
#define B_PIN	GPIO_PIN_1
//pin 4
#define C_PIN	GPIO_PIN_5
//pin 2
#define D_PIN	GPIO_PIN_6
//pin 1
#define E_PIN   GPIO_PIN_7
//pin 10
#define F_PIN	GPIO_PIN_8
//pin 5
#define G_PIN	GPIO_PIN_9
//pin 3
#define DP_PIN	GPIO_PIN_10

#define SEGMENTS_PORT GPIOB

#define ALL_SEGMENTS  (A_PIN | B_PIN | C_PIN | D_PIN | E_PIN | F_PIN | G_PIN | DP_PIN)

#ifdef TYPE_A
	#define SEGMENT_SET   GPIO_PIN_SET
	#define SEGMENT_RESET   GPIO_PIN_RESET
#else
	#define SEGMENT_SET   GPIO_PIN_RESET
	#define SEGMENT_RESET   GPIO_PIN_SET
#endif

//pin 12
#define DIG1_PIN	GPIO_PIN_15
//pin 9
#define DIG2_PIN	GPIO_PIN_14
//pin 8
#define DIG3_PIN	GPIO_PIN_13
//pin 6
#define DIG4_PIN	GPIO_PIN_12
#define ALL_DIGS	(DIG1_PIN | DIG2_PIN | DIG3_PIN | DIG4_PIN)
#define DIG_PORT GPIOB
#ifdef TYPE_A
	#define DIG_SET   GPIO_PIN_RESET
	#define DIG_RESET   GPIO_PIN_SET
#else
	#define DIG_SET   GPIO_PIN_SET
	#define DIG_RESET   GPIO_PIN_RESET
#endif

#define NUMBER0	A_PIN | B_PIN | C_PIN | D_PIN | E_PIN | F_PIN
#define NUMBER1	B_PIN | C_PIN
#define NUMBER2	A_PIN | B_PIN | E_PIN | D_PIN | G_PIN
#define NUMBER3	A_PIN | B_PIN | C_PIN | D_PIN | G_PIN
#define NUMBER4	F_PIN | B_PIN | C_PIN | G_PIN
#define NUMBER5	A_PIN | C_PIN | D_PIN | F_PIN | G_PIN
#define NUMBER6	A_PIN | C_PIN | D_PIN | E_PIN | F_PIN | G_PIN
#define NUMBER7	A_PIN | B_PIN | C_PIN
#define NUMBER8	A_PIN | B_PIN | C_PIN | D_PIN | E_PIN | F_PIN | G_PIN
#define NUMBER9	A_PIN | B_PIN | C_PIN | D_PIN | F_PIN | G_PIN
#define ERROR 	A_PIN | D_PIN | E_PIN | F_PIN | G_PIN


void AK_FYQ5641_Init(void);

void AK_FYQ5641_Next(void);

void AK_FYQ5641_Value(uint16_t newValue);


#endif /* AK_FYQ5641_H_ */
