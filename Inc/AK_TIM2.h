/*
 * Timer2.h
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_TIM2_H_
#define AK_TIM2_H_

#include "stm32f1xx_hal.h"

void AK_TIM2_Init(void);

void AK_TIM2_Msp_Init(void);

#endif /* AK_TIM2_H_ */
